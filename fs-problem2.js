const fs = require("fs");
// const path=require("path");

function paragraph(path, callback) {
  fs.readFile(path, "utf-8", (err, data) => {
    // Read the content of the lipsum file
    if (err) {
      return callback(err);
    }
    const inUpperCase = data.toUpperCase(); // Convert the data to uppercase
    // console.log(inUpperCase);
    fs.writeFile("upperCase.txt", inUpperCase, "utf-8", (err) => {
      // Write the uppercase data to a new file (upperCase.txt)
      if (err) {
        return callback(err);
      }
      fs.appendFile("filenames.txt", "upperCase.txt\n", "utf-8", (err) => {
        // Append the filename to filenames.txt
        if (err) {
          return callback(err);
        }
        fs.readFile("upperCase.txt", "utf-8", (err, upperCaseData) => {
          // Read the content of the uppercase file
          if (err) {
            return callback(err);
          }
          const inLowerCase = upperCaseData.toLowerCase(); // Convert the content to lowercase
          const splitContent = inLowerCase.split(".").filter(Boolean); // Split the content into sentences
          const formattedContenet = splitContent.join("\n"); // Format and join with newlines
          // console.log(formattedContenet);

          fs.writeFile("lowerCaseSentence.txt", formattedContenet, (err) => {
            // Write the formatted content to lowerCaseSentence.txt
            if (err) {
              return callback(err);
            }
            fs.appendFile(
              "filenames.txt",
              "lowerCaseSentence.txt\n",
              "utf-8",
              (err) => {
                // Append the lowerCaseSentence.txt to filenames.txt
                if (err) {
                  return callback(err);
                }
                fs.readFile(
                  "lowerCaseSentence.txt",
                  "utf-8",
                  (err, lowerCaseData) => {
                    // Read the content of the lowercase sentence file
                    if (err) {
                      return callback(err);
                    }
                    // console.log(lowerCaseData);
                    const sortedData = lowerCaseData
                      .split("\n")
                      .sort()
                      .join("\n"); // Split the content into lines, sort, and join with newlines
                    // console.log(sortedData);
                    fs.writeFile(
                      "sortedSentence.txt",
                      sortedData,
                      "utf-8",
                      (err) => {
                        if (err) {
                          return callback(err);
                        }
                        fs.appendFile(
                          "filenames.txt",
                          "sortedSentence.txt\n",
                          "utf-8",
                          (err) => {
                            // Appemd the file to filenames.txt
                            if (err) {
                              return callback(err);
                            }
                            fs.readFile(
                              "filenames.txt",
                              "utf-8",
                              (err, fileNamesData) => {
                                // Read the filenames.txt
                                if (err) {
                                  return callback(err);
                                }
                                const files = fileNamesData
                                  .split("\n")
                                  .filter(Boolean);
                                files.forEach((fileName) => {
                                  // Split the content into filenames, filter empty values, and delete each file
                                  fs.unlink(fileName, (err) => {
                                    if (err) {
                                      return callback(err);
                                    }
                                    // console.log("Deleted all files in one go");
                                  });
                                });
                                console.log("Deleted all files in one go");
                              }
                            );
                          }
                        );
                      }
                    );
                  }
                );
              }
            );
          });
        });
      });
    });
  });
}

module.exports = paragraph; //Exported the paragraph function
