const fs = require("fs"); //Imported fs node module
const path = require("path"); //Imported path module

function createDirAndAddFiles(dirPath, noOfFiles, callback) {
  // Function to create a directory and add specified number of files with random data
  fs.mkdir(dirPath, (err) => {
    if (err) {
      return callback(err); // Return error to the callback if directory creation does not happen
    }

    let count = 0;
    for (let index = 1; index <= noOfFiles; index++) {
      // Loop to create files and write random data to them
      let fileName = `file${index}.json`;
      let fileDestination = path.join(dirPath, fileName); // Join the path of directory to file
      let data = {
        data: Math.floor(Math.random() * 100),
      };
      fs.writeFile(fileDestination, JSON.stringify(data), (err) => {
        // Write data to the file
        if (err) {
          return callback(err);
        }
      });
      count++;
    }
    // console.log(count);
    if (count === noOfFiles) {
      // to check if all files are created or not before passing it to the callback
      callback(
        null,
        `Successfully created the directory and added the files inside the directory`
      );
    }
  });
}

function deleteFiles(dirPath, noOfFiles, callback) {
  // Function to delete specified number of files from a directory

  fs.readdir(dirPath, (err, files) => {
    // Return error to the callback if it does not read directory
    if (err) {
      return callback(err);
    }
    let count = noOfFiles;
    files.forEach((file) => {
      // Loop through files and delete each one
      let fileDestination = path.join(dirPath, file);
      fs.unlink(fileDestination, (err) => {
        if (err) {
          console.log(err);
        }
      });
      count--;
    });
    // console.log(count);
    if (count === 0) {
      // to check if all files are deleted successfully before passing it to the callback
      callback(null, `Succesfully deleted all the files`);
    }
  });
}

module.exports = {
  // Exported functions for external use
  createFiles: createDirAndAddFiles,
  deleteFiles: deleteFiles,
};
