const { createFiles, deleteFiles } = require("../fs-problem1.js"); //Imported both functions from problem1.cjs file

let path = "../Random";
let noOfFiles = 8;
createFiles(path, noOfFiles, (err, message) => {
  // Call the createFiles function
  if (err) {
    console.error(err); // give error if directory creation or file writing fails
  } else {
    console.log(message); // give success message if files are created successfully
    deleteFiles(path, noOfFiles, (err, message) => {
      // Call the deleteFiles function
      if (err) {
        console.error(err); // give error if it can not detect file
      } else {
        console.log(message); // give success message if files are deleted
      }
    });
  }
});
